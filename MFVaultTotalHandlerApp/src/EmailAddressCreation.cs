﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;
using System.Globalization;

namespace MFVaultTotalHandlerApp
{
    public partial class VaultApplication : VaultApplicationBase
    {
        [MFObjType(Required = true)]
        MFIdentifier OT_Email_Address = "B.OT.Email_Address";

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Email_Type = "B.PD.Email_Type";

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Email_Address = "B.PD.Email_Address";

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, ObjectType = "B.OT.Email_Address")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, ObjectType = "B.OT.Email_Address")]
        public void onEmailAddressCreated(EventHandlerEnvironment env)
        {
            try
            {
                SysUtils.ReportToEventLog("onEmailAddressCreated :Started", System.Diagnostics.EventLogEntryType.Information);

                PropertyValue prpEmailType = GetProperty(env, PD_Email_Type);
                PropertyValue prpParent = GetProperty(env, GetObjectTypeOwnerPropertyID(env.Vault, OT_Email_Address));
                PropertyValue prpEmailAddress = GetProperty(env, PD_Email_Address);

                if (prpParent.Value.IsNULL() == false)
                {
                    ObjID objId = new ObjID();
                    objId.SetIDs(OT_Contact, prpParent.Value.GetLookupID());
                    ObjVer contact = env.Vault.ObjectOperations.GetLatestObjVer(objId, true, true);

                    PropertyValue prpOldEmailType = null;

                    if (env.ObjVer.Version - 1 > 0)
                    {
                        ObjVer oldVersion = new ObjVer();
                        oldVersion.SetIDs(OT_Email_Address, env.ObjVer.ObjID.ID, env.ObjVer.Version - 1);
                        prpOldEmailType = env.Vault.ObjectPropertyOperations.GetProperty(oldVersion, PD_Email_Type);
                    }

                    if (prpEmailType.TypedValue.DisplayValue == "E-mail")
                    {
                        PropertyValue prpContactToBeSet = GetProperty(env.Vault, contact, PD_Email_Address);
                        ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(contact.ObjID);
                        prpContactToBeSet.Value.SetValue(MFDataType.MFDatatypeText, prpEmailAddress.Value.DisplayValue);
                        env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpContactToBeSet);
                        env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
                    }
                    else if(prpOldEmailType != null && prpOldEmailType.Value.DisplayValue == "E-mail")
                    {
                        PropertyValue prpContactToBeSet = GetProperty(env.Vault, contact, PD_Email_Address);
                        ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(contact.ObjID);
                        prpContactToBeSet.Value.SetValue(MFDataType.MFDatatypeText, null);
                        env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpContactToBeSet);
                        env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
                    }


                    SysUtils.ReportToEventLog("onEmailAddressCreated :Completed", System.Diagnostics.EventLogEntryType.Information);
                }
            }
            catch (Exception ex)
            {
                SysUtils.ReportToEventLog("Error on onEmailAddressCreated :" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }

        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, ObjectType = "B.OT.Email_Address")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDeleteObject, ObjectType = "B.OT.Email_Address")]
        public void onEmailAddressDeleted(EventHandlerEnvironment env)
        {
            try
            {
                SysUtils.ReportToEventLog("onEmailAddressDeleted :Started", System.Diagnostics.EventLogEntryType.Information);

                PropertyValue prpEmailType = GetProperty(env.Vault, env.ObjVer, PD_Email_Type);
                PropertyValue prpParentContact = GetProperty(env.Vault, env.ObjVer, GetObjectTypeOwnerPropertyID(env.Vault, OT_Email_Address));

                if (prpParentContact.Value.IsNULL() == false)
                {
                    ObjID objId = new ObjID();
                    objId.SetIDs(OT_Contact, prpParentContact.Value.GetLookupID());
                    ObjVer contact = env.Vault.ObjectOperations.GetLatestObjVer(objId, true, true);

                    if (prpEmailType.TypedValue.DisplayValue == "E-mail")
                    {
                        PropertyValue prpParentEmail = GetProperty(env.Vault, contact, PD_Email_Address);
                        prpParentEmail.Value.SetValue(MFDataType.MFDatatypeText, null);

                        ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(contact.ObjID);
                        env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpParentEmail);

                        env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
                    }
                }

                SysUtils.ReportToEventLog("onEmailAddressDeleted :Finished", System.Diagnostics.EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                SysUtils.ReportToEventLog("Error on onEmailAddressDeleted :" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }
    }
}
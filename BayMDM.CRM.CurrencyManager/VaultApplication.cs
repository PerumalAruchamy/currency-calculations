﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;
using System.Globalization;
using MFiles.VAF.Configuration;

namespace BayMDM.CRM.CurrencyManager
{

    /// <summary>
    /// Simple vault application to demonstrate VAF.
    /// </summary>
    public class VaultApplication : VaultApplicationBase
    {
        /// <summary>
        /// Simple configuration member. MFConfiguration-attribute will cause this member to be loaded from the named value storage from the given namespace and key.
        /// Here we override the default alias of the Configuration class with a default of the config member.
        /// The default will be written in the named value storage, if the value is missing.
        /// Use Named Value Manager to change the configurations in the named value storage.
        /// </summary>

        bool IsValidCheckIn = true;
        bool IsCalledFromLine = false;

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_Invoice_Line_TotalAmount_PropertyDef = "B.PD.Total_Amount"; //"B.PD.Total_Line_Amount";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_Invoice_TotalAmount_PropertyDef = "B.PD.Total_Amount";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_Invoice_NetAmount_PropertyDef = "B.PD.Net_Amount";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_Invoice_TaxDetails_PropertyDef = "B.PD.Tax_Details";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_Invoice_ID_PropertyDef = "B.PD.AP_Invoice_#";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_Invoice_Line_TAX_GROUP_NAME_PD = "B.PD.Tax_Group_Name";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_InvoiceLine_TAX_Group_PD = "B.PD.Tax_Group";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_InvoiceLine_TAX_Details_PD = "B.PD.Tax_Details";

        [MFObjType(Required = true)]
        MFIdentifier OT_TAX_GROUP = "B.OT.Tax_Group";

        [MFObjType(Required = true)]
        MFIdentifier OT_TAX_RATE = "B.OT.Tax_Rate";

        [MFPropertyDef(Required = true)]
        MFIdentifier AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD = "B.PD.Tax_Rates";

        [MFPropertyDef(Required = true)]
        MFIdentifier TAX_RATE_PD = "B.PD.Tax_Rate_%";

        [MFPropertyDef(Required = true)]
        MFIdentifier DECIMAL_PLACE_PD = "B.PD.Decimal_Places";

        [MFObjType(Required = true)]
        MFIdentifier OT_TAXT_DETAIL = "B.OT.Tax_Detail";

        [MFPropertyDef(Required = true)]
        MFIdentifier TAX_DETAIL_NAME_PD = "B.PD.Tax_Detail_Name";

        [MFPropertyDef(Required = true)]
        MFIdentifier TAX_DETAIL_TAX_RATE_PD = "B.PD.Tax_Rate";

        [MFPropertyDef(Required = true)]
        MFIdentifier TAX_AMOUNT_PD = "B.PD.Tax_Amount";

        [MFPropertyDef(Required = true)]
        MFIdentifier Net_Line_Amount_PD = "B.PD.Net_Amount";

        [MFPropertyDef(Required = true)]
        MFIdentifier Line_Items_PD = " B.PD.Line_Items";

        [MFPropertyDef(Required = true)]
        MFIdentifier Quantity_PD = "Bay.Quantity.Prop";

        [MFPropertyDef(Required = true)]
        MFIdentifier Unit_Price_PD = "B.PD.Unit_Price";

        [MFPropertyDef(Required = true)]
        MFIdentifier Transaction_Amount_PD = "B.PD.Transaction_Amount";

        [MFPropertyDef(Required = true)]
        MFIdentifier Line_PD = "B.PD.Line_#";

        [MFPropertyDef(Required = true)]
        MFIdentifier B_PD_Item = "B.PD.Item";

        [MFPropertyDef(Required = true)]
        MFIdentifier B_PD_Currency = "B.PD.Currency";

        [MFPropertyDef(Required = true)]
        MFIdentifier B_PD_Culture = "B.PD.Culture";

        [MFPropertyDef(Required = true)]
        MFIdentifier B_PD_Remaining_Balance = "B.PD.Remaining_Balance";

        [MFPropertyDef(Required = true)]
        MFIdentifier B_PD_Net_Amount = "B.PD.Net_Amount";

        [MFObjType]
        MFIdentifier OT_AP_INVOICE_LINE = "B.OT.Line_Item";

        [MFObjType]
        MFIdentifier OT_CURRENCY = "B.OT.Currency";

        [MFPropertyDef]
        MFIdentifier B_PD_AP_Invoice = " B.PD.AP_Invoice";

        [MFPropertyDef]
        MFIdentifier B_PD_AR_Invoice = " B.PD.AR_Invoice";

        [MFClass]
        MFIdentifier OT_AP_INVOICE = "B.DC.AP_Invoice";

        [MFObjType]
        MFIdentifier OT_AR_INVOICE = 0; //"B.OT.AR_Invoice";

        [MFObjType]
        MFIdentifier OT_AR_INVOICE_LINE = "B.OT.Line_Item"; // "B.OT.AR_Invoice_Line";

        [MFClass]
        MFIdentifier OC_TAX_DETAIL = "B.OC.Tax_Detail";


        [MFClass(Required = true)]
        MFIdentifier OC_AP_INVOICE_LINE = "B.OC.AP_Line_Item"; //"B.OC.AP_Invoice_Line";


        [MFValueList]
        MFIdentifier VL_CULTURES = "B.VL.Cultures";

        /// <summary>
        /// The method, that is run when the vault goes online.
        /// </summary>
        protected override void StartApplication()
        {
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.OC.Currency_Calculation")]
        [EventHandler(MFEventHandlerType.MFEventHandlerAfterCheckInChanges, Class = "B.OC.Currency_Calculation")]
        public void onCalculateCurrencyCreated(EventHandlerEnvironment env)
        {
            CalculateCurrency(env);
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.OC.Purchase_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.OC.Sales_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.OC.Purchase_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.OC.Sales_Item")]
        public void onCreatingPurchaseAndSales(EventHandlerEnvironment env)
        {
            try
            {
                if (!IsValidCheckIn)
                    return;

                SysUtils.ReportToEventLog("START: onCreatingPurchaseAndSales", System.Diagnostics.EventLogEntryType.Information);

                Vault vault = env.Vault;
                ObjVer objVer = env.ObjVer;
                CultureInfo culture = null;
                double UnitPrice;

                //calculating unit price
                PropertyValue prpUnitPrice = GetProperty(env, Unit_Price_PD);
                PropertyValue prpCurrency = GetProperty(env, B_PD_Currency);

                if (prpCurrency.Value.IsNULL() == false)
                {
                    ObjID objId = new ObjID();
                    objId.SetIDs(OT_CURRENCY, prpCurrency.Value.GetLookupID());
                    ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue prpParentCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
                    culture = new CultureInfo(prpParentCulture.TypedValue.DisplayValue);
                }
                else
                {
                    culture = new CultureInfo("en-US");
                }

                double.TryParse(prpUnitPrice.TypedValue.DisplayValue, NumberStyles.Currency, culture, out UnitPrice);

                prpUnitPrice.TypedValue.SetValue(MFDataType.MFDatatypeText, UnitPrice.ToString("c", culture));
                vault.ObjectPropertyOperations.SetProperty(objVer, prpUnitPrice);
                SysUtils.ReportToEventLog("END: onCreatingPurchaseAndSales", System.Diagnostics.EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog("Error on onCreatingPurchaseAndSales: " + ex);
            }
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.OC.Transaction_Record")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.OC.Transaction_Record")]
        public void onCreatingTransactionRecord(EventHandlerEnvironment env)
        {
            try
            {
                if (!IsValidCheckIn)
                    return;

                SysUtils.ReportToEventLog("START: onCreatingTransactionRecord", System.Diagnostics.EventLogEntryType.Information);

                Vault vault = env.Vault;
                ObjVer objVer = env.ObjVer;
                CultureInfo culture = null;
                double UnitPrice;

                //calculating unit price
                PropertyValue prpTransactionAmount = GetProperty(env, Transaction_Amount_PD);
                PropertyValue prpCurrency = GetProperty(env, B_PD_Currency);

                if (prpCurrency.Value.IsNULL() == false)
                {
                    ObjID objId = new ObjID();
                    objId.SetIDs(OT_CURRENCY, prpCurrency.Value.GetLookupID());
                    ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue prpParentCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
                    culture = new CultureInfo(prpParentCulture.TypedValue.DisplayValue);
                }
                else
                {
                    culture = new CultureInfo("en-US");
                }

                double.TryParse(prpTransactionAmount.TypedValue.DisplayValue, NumberStyles.Currency, culture, out UnitPrice);

                prpTransactionAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, UnitPrice.ToString("c", culture));
                vault.ObjectPropertyOperations.SetProperty(objVer, prpTransactionAmount);

                SysUtils.ReportToEventLog("END: onCreatingTransactionRecord", System.Diagnostics.EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog("Error on onCreatingTransactionRecord: " + ex);
            }
        }


        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.OC.AP_Line_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.OC.AP_Line_Item")]
        public void onAPLineCheckInOrCreation(EventHandlerEnvironment env)
        {
            try
            {
                TotalUpdate(env, 0, OT_AP_INVOICE_LINE, B_PD_AP_Invoice);
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);
            }
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.OC.AR_Line_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.OC.AR_Line_Item")]
        public void onARLineCheckinOrCreation(EventHandlerEnvironment env)
        {
            try
            {
                TotalUpdate(env, 0, OT_AR_INVOICE_LINE, B_PD_AR_Invoice);
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);
            }
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerAfterCreateNewObjectFinalize, Class = "B.DC.AP_Store_Receipt")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.DC.AP_Store_Receipt")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.DC.AP_Store_Receipt")]
        public void onAPStoreReceiptCreated(EventHandlerEnvironment env)
        {
            onReceiptCreated(env);
            IsCalledFromLine = false;
        }

        private void onReceiptCreated(EventHandlerEnvironment env)
        {
            try
            {
                SysUtils.ReportToEventLog("START: onReceiptCreated", System.Diagnostics.EventLogEntryType.Information);

                Vault vault = env.Vault;
                ObjVer objVer = env.ObjVer;
                ObjID objId;
                double taxAmount = 0;
                double totalTaxAmount = 0;
                double totalTaxPercentage = 0;
                CultureInfo culture = null;
                List<int> taxDetailLookup = new List<int>();
                PropertyValue taxGroupValue = GetProperty(env, AP_InvoiceLine_TAX_Group_PD);
                PropertyValue prptaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);

                PropertyValue prpTotal = GetProperty(env, AP_Invoice_TotalAmount_PropertyDef);
                PropertyValue prpNetAmount = GetProperty(env, AP_Invoice_NetAmount_PropertyDef);

                PropertyValue prpCurrency = GetProperty(env, B_PD_Currency);
                if (prpCurrency.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_CURRENCY, prpCurrency.Value.GetLookupID());
                    ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue prpCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
                    culture = new CultureInfo(prpCulture.TypedValue.DisplayValue);
                }
                else
                {
                    culture = new CultureInfo("en-US");
                }

                Dictionary<int, double> trWiseTaxTotal = new Dictionary<int, double>();
                taxDetailLookup = new List<int>();

                double lineNetAmount = 0.0;
                double lineTotalAmount;

                double.TryParse(prpNetAmount.TypedValue.DisplayValue, NumberStyles.Currency, culture, out lineNetAmount);
                double.TryParse(prpTotal.TypedValue.DisplayValue, NumberStyles.Currency, culture, out lineTotalAmount);

                prpNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineNetAmount.ToString("c", culture));
                vault.ObjectPropertyOperations.SetProperty(objVer, prpNetAmount);

                ObjVer objTaxGroup = null;
                if (taxGroupValue?.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_TAX_GROUP, taxGroupValue.Value.GetLookupID());
                    objTaxGroup = vault.ObjectOperations.GetLatestObjVer(objId, false, true);
                }
                if (objTaxGroup != null)
                {
                    PropertyValue pvTaxRates = vault.ObjectPropertyOperations.GetProperty(objTaxGroup, AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD);

                    if (pvTaxRates == null)
                    {
                        SysUtils.ReportToEventLog("PD Tax Rates not found :" + AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD, System.Diagnostics.EventLogEntryType.Information);
                    }

                    Lookups lookups = pvTaxRates.TypedValue.GetValueAsLookups();

                    foreach (Lookup item in lookups)
                    {
                        objId = new ObjID();
                        objId.SetIDs(OT_TAX_RATE, item.Item);
                        ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                        PropertyValue taxRateItem = vault.ObjectPropertyOperations.GetProperty(taxRate, TAX_RATE_PD);

                        double taxPercentage;
                        double.TryParse(taxRateItem.TypedValue.DisplayValue.Replace("%", string.Empty), out taxPercentage);

                        int decimalPlace;
                        PropertyValue taxDecimalPlace = vault.ObjectPropertyOperations.GetProperty(taxRate, DECIMAL_PLACE_PD);
                        int.TryParse(taxDecimalPlace.TypedValue.DisplayValue, out decimalPlace);

                        if (lineNetAmount != 0)
                        {
                            taxAmount = (lineNetAmount * taxPercentage) / 100;
                            taxAmount = Math.Round(taxAmount, decimalPlace);
                            totalTaxAmount += taxAmount;
                            lineTotalAmount = lineNetAmount + totalTaxAmount;
                        }
                        else
                        {
                            totalTaxPercentage += (taxPercentage) / 100;
                        }
                    }
                    if (lineNetAmount == 0)
                    {
                        lineNetAmount = lineTotalAmount / (1 + totalTaxPercentage);
                        lineNetAmount = Math.Round(lineNetAmount);
                    }
                    foreach (Lookup item in lookups)
                    {
                        objId = new ObjID();
                        objId.SetIDs(OT_TAX_RATE, item.Item);
                        ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                        PropertyValue taxRateItem = vault.ObjectPropertyOperations.GetProperty(taxRate, TAX_RATE_PD);

                        double taxPercentage;
                        double.TryParse(taxRateItem.TypedValue.DisplayValue.Replace("%", string.Empty), out taxPercentage);

                        taxAmount = (lineNetAmount * taxPercentage) / 100;

                        if (trWiseTaxTotal.ContainsKey(objId.ID))
                        {
                            trWiseTaxTotal[objId.ID] += taxAmount;
                        }
                        else
                        {
                            trWiseTaxTotal.Add(objId.ID, taxAmount);
                        }
                    }
                }
                else
                {
                    lineTotalAmount = lineNetAmount;
                    if (prptaxDetails?.Value.IsNULL() == false)
                    {
                        Lookups lookups = prptaxDetails.TypedValue.GetValueAsLookups();
                        foreach (Lookup item in lookups)
                        {
                            objId = new ObjID();
                            objId.SetIDs(OT_TAXT_DETAIL, item.Item);
                            ObjVer objTaxDetail = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                            taxDetailLookup.Remove(objTaxDetail.ObjID.ID);
                        }
                        PropertyValue prpTaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);
                        prpTaxDetails.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                        vault.ObjectPropertyOperations.SetProperty(objVer, prpTaxDetails);
                    }
                }
                prpTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, lineTotalAmount.ToString("c", culture));
                prpNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineNetAmount.ToString("c", culture));

                if (trWiseTaxTotal.Count > 0)
                {
                    PropertyValue prpParentTaxDetail = GetProperty(env, AP_Invoice_TaxDetails_PropertyDef);

                    foreach (var dicItem in trWiseTaxTotal)
                    {
                        PropertyValues prpsTaxDetail = new PropertyValues();

                        PropertyValue prpTaxClass = new PropertyValue();
                        prpTaxClass.PropertyDef = 100;
                        prpTaxClass.TypedValue.SetValue(MFDataType.MFDatatypeLookup, 245); //246
                        prpsTaxDetail.Add(-1, prpTaxClass);

                        PropertyValue prpTaxRate = new PropertyValue();
                        prpTaxRate.PropertyDef = TAX_DETAIL_TAX_RATE_PD;
                        prpTaxRate.TypedValue.SetValue(MFDataType.MFDatatypeLookup, dicItem.Key);
                        prpsTaxDetail.Add(-1, prpTaxRate);

                        PropertyValue prpTaxAmount = new PropertyValue();
                        prpTaxAmount.PropertyDef = TAX_AMOUNT_PD;
                        prpTaxAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, dicItem.Value.ToString("c", culture));
                        prpsTaxDetail.Add(-1, prpTaxAmount);

                        var obj1 = vault.ObjectOperations.CreateNewObject(OT_TAXT_DETAIL, prpsTaxDetail);
                        var checkinObj1 = vault.ObjectOperations.CheckIn(obj1.ObjVer);
                        taxDetailLookup.Add(checkinObj1.ObjVer.ObjID.ID);
                    }
                    prpParentTaxDetail.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                    vault.ObjectPropertyOperations.SetProperty(env.ObjVer, prpParentTaxDetail);
                }

                vault.ObjectPropertyOperations.SetProperty(env.ObjVer, prpTotal);
                vault.ObjectPropertyOperations.SetProperty(env.ObjVer, prpNetAmount);

                SysUtils.ReportToEventLog("END: onApInvoiceCreated", System.Diagnostics.EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog("Error on onReceiptCreated: " + ex);
            }
            finally
            {
                IsValidCheckIn = true;
            }
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerAfterCreateNewObjectFinalize, Class = "B.DC.AP_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.DC.AP_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.DC.AP_Invoice")]
        public void onAPInvoiceCreated(EventHandlerEnvironment env)
        {
            onHeaderCreated(env, 243, OT_AP_INVOICE_LINE);
            IsCalledFromLine = false;
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerAfterCreateNewObjectFinalize, Class = "B.DC.AR_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, Class = "B.DC.AR_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, Class = "B.DC.AR_Invoice")]
        public void onARInvoiceCreated(EventHandlerEnvironment env)
        {
            onHeaderCreated(env, 244, OT_AR_INVOICE_LINE);
            IsCalledFromLine = false;
        }
        private void onHeaderCreated(EventHandlerEnvironment env, int lineClass, int lineObjectType)
        {
            try
            {
                if (IsCalledFromLine)
                    return;

                SysUtils.ReportToEventLog("START: onApInvoiceCreated", System.Diagnostics.EventLogEntryType.Information);

                Vault vault = env.Vault;
                ObjVer objVer = env.ObjVer;
                ObjID objId;
                double parentTotalAmount = 0;
                double parentNetAmount = 0;
                double parentTaxAmount = 0;
                double totalTaxAmount = 0;
                double totalTaxPercentage = 0;
                double lineNetAmount = 0.0;
                double lineTotalAmount = 0.0;

                CultureInfo culture = null;
                List<int> taxDetailLookup = new List<int>();
                PropertyValue taxGroupValue = GetProperty(env, AP_InvoiceLine_TAX_Group_PD);
                PropertyValue prptaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);

                PropertyValue prpParentTotal = GetProperty(env, AP_Invoice_TotalAmount_PropertyDef);
                PropertyValue prpParentNetAmount = GetProperty(env, AP_Invoice_NetAmount_PropertyDef);

               
                PropertyValue prpCurrency = GetProperty(env, B_PD_Currency);
                if (prpCurrency.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_CURRENCY, prpCurrency.Value.GetLookupID());
                    ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue prpCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
                    if (!String.IsNullOrEmpty(prpCulture.TypedValue.DisplayValue))
                    {
                        culture = new CultureInfo(prpCulture.TypedValue.DisplayValue);
                    }
                    else
                    {
                        culture = new CultureInfo("en-US");
                    }
                }
                else
                {
                    culture = new CultureInfo("en-US");
                }
                GetDecimalFromString(env, prpParentNetAmount.TypedValue.DisplayValue, culture, out lineNetAmount);
                GetDecimalFromString(env, prpParentTotal.TypedValue.DisplayValue, culture, out lineTotalAmount);


                Dictionary<int, double> trWiseTaxTotal = new Dictionary<int, double>();
                taxDetailLookup = new List<int>();
                ObjVer objTaxGroup = null;
                if (taxGroupValue?.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_TAX_GROUP, taxGroupValue.Value.GetLookupID());
                    objTaxGroup = vault.ObjectOperations.GetLatestObjVer(objId, false, true);
                }

                PropertyValue prpLineItems = GetProperty(env, Line_Items_PD);

                Lookups lookups = prpLineItems.TypedValue.GetValueAsLookups();

                if (lookups.Count == 0)
                {
                    //double lineNetAmount = 0.0;
                    //double lineTotalAmount;

                    //double.TryParse(prpParentNetAmount.TypedValue.DisplayValue.Replace('€', ' ').TrimEnd(), NumberStyles.Currency, culture, out lineNetAmount);
                    //double.TryParse(prpParentTotal.TypedValue.DisplayValue, NumberStyles.Currency, culture, out lineTotalAmount);

                    // lineTotalAmount = lineNetAmount;

                    prpParentNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineNetAmount.ToString("c", culture));
                    vault.ObjectPropertyOperations.SetProperty(objVer, prpParentNetAmount);

                   
                    if (objTaxGroup != null)
                    {
                        PropertyValue pvTaxRates = vault.ObjectPropertyOperations.GetProperty(objTaxGroup, AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD);

                        if (pvTaxRates == null)
                        {
                            SysUtils.ReportToEventLog("PD Tax Rates not found :" + AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD, System.Diagnostics.EventLogEntryType.Information);
                        }

                        lookups = pvTaxRates.TypedValue.GetValueAsLookups();

                        foreach (Lookup item in lookups)
                        {
                            objId = new ObjID();
                            objId.SetIDs(OT_TAX_RATE, item.Item);
                            ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                            PropertyValue taxRateItem = vault.ObjectPropertyOperations.GetProperty(taxRate, TAX_RATE_PD);

                            double taxPercentage;
                            double.TryParse(taxRateItem.TypedValue.DisplayValue.Replace("%", string.Empty), out taxPercentage);

                            int decimalPlace;
                            PropertyValue taxDecimalPlace = vault.ObjectPropertyOperations.GetProperty(taxRate, DECIMAL_PLACE_PD);
                            int.TryParse(taxDecimalPlace.TypedValue.DisplayValue, out decimalPlace);

                            if (lineNetAmount != 0)
                            {
                                parentTaxAmount = (lineNetAmount * taxPercentage) / 100;
                                parentTaxAmount = Math.Round(parentTaxAmount, decimalPlace);
                                totalTaxAmount += parentTaxAmount;
                                lineTotalAmount = lineNetAmount + totalTaxAmount;
                            }
                            else
                            {
                                totalTaxPercentage += (taxPercentage) / 100;
                            }
                        }
                        if (lineNetAmount == 0)
                        {
                            lineNetAmount = lineTotalAmount / (1 + totalTaxPercentage);
                            lineNetAmount = Math.Round(lineNetAmount);
                        }
                        foreach (Lookup item in lookups)
                        {
                            objId = new ObjID();
                            objId.SetIDs(OT_TAX_RATE, item.Item);
                            ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                            PropertyValue taxRateItem = vault.ObjectPropertyOperations.GetProperty(taxRate, TAX_RATE_PD);

                            double taxPercentage;
                            double.TryParse(taxRateItem.TypedValue.DisplayValue.Replace("%", string.Empty), out taxPercentage);

                            parentTaxAmount = (lineNetAmount * taxPercentage) / 100;

                            if (trWiseTaxTotal.ContainsKey(objId.ID))
                            {
                                trWiseTaxTotal[objId.ID] += parentTaxAmount;
                            }
                            else
                            {
                                trWiseTaxTotal.Add(objId.ID, parentTaxAmount);
                            }
                        }
                    }
                    else
                    {
                        if (prpParentTotal.TypedValue.DisplayValue != lineTotalAmount.ToString() && lineNetAmount != 0)
                        {
                            lineTotalAmount = lineNetAmount;
                        }
                        else if (prpParentNetAmount.TypedValue.DisplayValue != lineNetAmount.ToString() && lineTotalAmount != 0)
                        {
                            lineNetAmount = lineTotalAmount;
                        }
                        else if (lineNetAmount == 0 && lineTotalAmount != 0)
                        {
                            lineNetAmount = lineTotalAmount;
                        }
                        else if (lineTotalAmount == 0 && lineNetAmount != 0)
                        {
                            lineTotalAmount = lineNetAmount;
                        }                      
                    }
                    prpParentTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, lineTotalAmount.ToString("c", culture));
                    prpParentNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineNetAmount.ToString("c", culture));
                }
                else
                {
                    foreach (Lookup item in lookups)
                    {
                        objId = new ObjID();
                        objId.SetIDs(OT_AP_INVOICE_LINE, item.Item);
                        ObjVer lineItem = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                        PropertyValue lineItemAmount = vault.ObjectPropertyOperations.GetProperty(lineItem, Net_Line_Amount_PD);

                        double exitingLineTotalAmount = 0;
                        double exitingLineNetAmount = 0;

                        PropertyValue prpExistingLineTotal = GetProperty(vault, lineItem, AP_Invoice_TotalAmount_PropertyDef);
                        double.TryParse(prpExistingLineTotal.Value.DisplayValue, NumberStyles.Currency, culture, out exitingLineTotalAmount);
                        parentTotalAmount += exitingLineTotalAmount;

                        PropertyValue prpExistingNet = GetProperty(vault, lineItem, Net_Line_Amount_PD);
                        double.TryParse(prpExistingNet.Value.DisplayValue, NumberStyles.Currency, culture, out exitingLineNetAmount);
                        parentNetAmount += exitingLineNetAmount;

                        taxGroupValue = GetProperty(env.Vault, lineItem, AP_InvoiceLine_TAX_Group_PD);

                        if (taxGroupValue.Value.IsNULL() == false)
                        {
                            objId = new ObjID();
                            objId.SetIDs(OT_TAX_GROUP, taxGroupValue.Value.GetLookupID());
                            ObjVer taxGroup = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                            PropertyValue pvTaxRates = GetProperty(env.Vault, taxGroup, AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD);

                            lookups = pvTaxRates.TypedValue.GetValueAsLookups();

                            foreach (Lookup lookup in lookups)
                            {
                                objId = new ObjID();
                                objId.SetIDs(OT_TAX_RATE, lookup.Item);
                                ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                                double taxRateAmount = 0;
                                Double.TryParse(GetProperty(vault, taxRate, TAX_RATE_PD).TypedValue.Value.ToString().Replace("%", ""), out taxRateAmount);
                                double tax;

                                int decimalPlace;
                                PropertyValue taxDecimalPlace = vault.ObjectPropertyOperations.GetProperty(taxRate, DECIMAL_PLACE_PD);
                                int.TryParse(taxDecimalPlace.TypedValue.DisplayValue, out decimalPlace);

                                tax = exitingLineNetAmount * taxRateAmount / 100;

                                tax = Math.Round(tax, decimalPlace);

                                if (trWiseTaxTotal.ContainsKey(objId.ID))
                                {
                                    trWiseTaxTotal[objId.ID] += tax;
                                }
                                else
                                {
                                    trWiseTaxTotal.Add(objId.ID, tax);
                                }
                            }
                        }
                    }


                    prpParentTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, parentTotalAmount.ToString("c", culture));
                    prpParentNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, parentNetAmount.ToString("c", culture));
                }
                //var objectVersionToBeUpdated = vault.ObjectOperations.CheckOut(env.ObjVer.ObjID);
                if (objTaxGroup == null)
                {
                    if (prptaxDetails?.Value.IsNULL() == false)
                    {
                        lookups = prptaxDetails.TypedValue.GetValueAsLookups();
                        foreach (Lookup item in lookups)
                        {
                            objId = new ObjID();
                            objId.SetIDs(OT_TAXT_DETAIL, item.Item);
                            ObjVer objTaxDetail = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                            taxDetailLookup.Remove(objTaxDetail.ObjID.ID);
                        }
                        PropertyValue prpTaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);
                        prpTaxDetails.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                        vault.ObjectPropertyOperations.SetProperty(objVer, prpTaxDetails);
                    }
                }

                if (trWiseTaxTotal.Count > 0)
                {
                    PropertyValue prpParentTaxDetail = GetProperty(env, AP_Invoice_TaxDetails_PropertyDef);

                    foreach (var dicItem in trWiseTaxTotal)
                    {
                        PropertyValues prpsTaxDetail = new PropertyValues();

                        PropertyValue prpTaxClass = new PropertyValue();
                        prpTaxClass.PropertyDef = 100;
                        prpTaxClass.TypedValue.SetValue(MFDataType.MFDatatypeLookup, 246);
                        prpsTaxDetail.Add(-1, prpTaxClass);

                        PropertyValue prpTaxRate = new PropertyValue();
                        prpTaxRate.PropertyDef = TAX_DETAIL_TAX_RATE_PD;
                        prpTaxRate.TypedValue.SetValue(MFDataType.MFDatatypeLookup, dicItem.Key);
                        prpsTaxDetail.Add(-1, prpTaxRate);

                        PropertyValue prpTaxAmount = new PropertyValue();
                        prpTaxAmount.PropertyDef = TAX_AMOUNT_PD;
                        prpTaxAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, dicItem.Value.ToString("c", culture));
                        prpsTaxDetail.Add(-1, prpTaxAmount);

                        var obj1 = vault.ObjectOperations.CreateNewObject(OT_TAXT_DETAIL, prpsTaxDetail);
                        var checkinObj1 = vault.ObjectOperations.CheckIn(obj1.ObjVer);
                        taxDetailLookup.Add(checkinObj1.ObjVer.ObjID.ID);
                    }
                    prpParentTaxDetail.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                    vault.ObjectPropertyOperations.SetProperty(env.ObjVer, prpParentTaxDetail);
                }

                vault.ObjectPropertyOperations.SetProperty(env.ObjVer, prpParentTotal);
                vault.ObjectPropertyOperations.SetProperty(env.ObjVer, prpParentNetAmount);

                SysUtils.ReportToEventLog("END: onApInvoiceCreated", System.Diagnostics.EventLogEntryType.Information);

            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog("Error on onApInvoiceCreated: " + ex);
            }
            finally
            {
                IsValidCheckIn = true;
            }
        }

        private void CalculateCurrency(EventHandlerEnvironment env)
        {

            if (!IsValidCheckIn)
                return;

            SysUtils.ReportToEventLog("START: CalculateCurrency", System.Diagnostics.EventLogEntryType.Information);

            Vault vault = env.Vault;
            ObjVer objVer = env.ObjVer;
            CultureInfo culture = null;
            double NetAmount;
            double qty;
            double TotalAmount;

            //calculating line amount
            PropertyValue prpNetAmount = GetProperty(env, B_PD_Net_Amount);
            PropertyValue prpCurrency = GetProperty(env, B_PD_Currency);
            PropertyValue prpQuantity = GetProperty(env, Quantity_PD);
            PropertyValue prpTotalAmount = GetProperty(env, AP_Invoice_Line_TotalAmount_PropertyDef);


            ObjID objId = new ObjID();
            objId.SetIDs(OT_CURRENCY, prpCurrency.Value.GetLookupID());
            ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

            PropertyValue prpParentCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
            culture = new CultureInfo(prpParentCulture.TypedValue.DisplayValue);

            double.TryParse(prpNetAmount.TypedValue.DisplayValue, NumberStyles.Currency, culture, out NetAmount);
            double.TryParse(prpQuantity.TypedValue.DisplayValue, out qty);

            TotalAmount = NetAmount * qty;

            prpNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, NetAmount.ToString("c", culture));
            vault.ObjectPropertyOperations.SetProperty(objVer, prpNetAmount);

            prpTotalAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, TotalAmount.ToString("c", culture));
            vault.ObjectPropertyOperations.SetProperty(objVer, prpTotalAmount);
        }

     

        private void TotalUpdate(EventHandlerEnvironment env, int headerObjectType, int lineObjectType, int invoiceProperty)
        {
            SysUtils.ReportToEventLog("START: TotalUpdate", System.Diagnostics.EventLogEntryType.Information);

            Vault vault = env.Vault;
            ObjVer objVer = env.ObjVer;
            ObjID objId;
            ObjVer parentObject = null; ;

            //calculating line amount
            PropertyValue parent = GetProperty(env, invoiceProperty);
            PropertyValue prpLineNetAmount = GetProperty(env, Net_Line_Amount_PD);
            PropertyValue prpLineTotal = GetProperty(env, AP_Invoice_TotalAmount_PropertyDef);
            PropertyValue prpLineCurrency = GetProperty(env, B_PD_Currency);
            CultureInfo culture = null;

            if (parent.Value.IsNULL() == false)
            {
                objId = new ObjID();
                objId.SetIDs(headerObjectType, parent.Value.GetLookupID());
                parentObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                if (parentObject == null)
                {
                    SysUtils.ReportToEventLog("parentObject not found ", System.Diagnostics.EventLogEntryType.Information);
                }

                PropertyValue prpParentCurrency = GetProperty(env.Vault, parentObject, B_PD_Currency);
                if (prpParentCurrency.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_CURRENCY, prpParentCurrency.Value.GetLookupID());
                    ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue prpParentCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
                    culture = new CultureInfo(prpParentCulture.TypedValue.DisplayValue);
                }
                else
                {
                    culture = new CultureInfo("en-US");
                }
            }
            else
            {
                culture = new CultureInfo("en-US");
            }

            double lineNetAmount = 0.0;
            double lineTotalAmount;

            GetDecimalFromString(env, prpLineNetAmount.TypedValue.DisplayValue, culture, out lineNetAmount);
            GetDecimalFromString(env, prpLineTotal.TypedValue.DisplayValue, culture, out lineTotalAmount);

            //prpLineNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineNetAmount.ToString("c", culture));
            //vault.ObjectPropertyOperations.SetProperty(objVer, prpLineNetAmount);

            //prpLineTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, lineTotalAmount.ToString("c", culture));
            //vault.ObjectPropertyOperations.SetProperty(objVer, prpLineTotal);

            PropertyValue taxGroupValue = GetProperty(env, AP_InvoiceLine_TAX_Group_PD);
            ObjVer objTaxGroup = null;

            PropertyValue prptaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);          
            if (taxGroupValue?.Value.IsNULL() == false)
            {
                objId = new ObjID();
                objId.SetIDs(OT_TAX_GROUP, taxGroupValue.Value.GetLookupID());
                objTaxGroup = vault.ObjectOperations.GetLatestObjVer(objId, false, true);
            }

            SearchCondition searchCondtion = new SearchCondition();
            SearchConditions conditions = new SearchConditions();

            List<int> taxDetailLookup = new List<int>();
            double lineTaxAmount = 0;
            double totalTaxAmount = 0;
            double totalTaxPercentage = 0;

            if (objTaxGroup != null)
            {
                PropertyValue pvTaxRates = vault.ObjectPropertyOperations.GetProperty(objTaxGroup, AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD);

                if (pvTaxRates == null)
                {
                    SysUtils.ReportToEventLog("PD Tax Rates not found :" + AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD, System.Diagnostics.EventLogEntryType.Information);
                }

                Lookups lookups = pvTaxRates.TypedValue.GetValueAsLookups();

                foreach (Lookup item in lookups)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_TAX_RATE, item.Item);
                    ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue taxRateItem = vault.ObjectPropertyOperations.GetProperty(taxRate, TAX_RATE_PD);

                    double taxPercentage;
                    double.TryParse(taxRateItem.TypedValue.DisplayValue.Replace("%", string.Empty), out taxPercentage);

                    int decimalPlace;
                    PropertyValue taxDecimalPlace = vault.ObjectPropertyOperations.GetProperty(taxRate, DECIMAL_PLACE_PD);
                    int.TryParse(taxDecimalPlace.TypedValue.DisplayValue, out decimalPlace);


                    if (lineNetAmount != 0)
                    {
                        lineTaxAmount = (lineNetAmount * taxPercentage) / 100;
                        lineTaxAmount = Math.Round(lineTaxAmount, decimalPlace);
                        totalTaxAmount += lineTaxAmount;
                        lineTotalAmount = lineNetAmount + totalTaxAmount;
                    }
                    else
                    {
                        totalTaxPercentage += (taxPercentage) / 100;
                    }
                }
                if (lineNetAmount == 0)
                {
                    lineNetAmount = lineTotalAmount / (1 + totalTaxPercentage);
                    lineNetAmount = Math.Round(lineNetAmount);
                }

                foreach (Lookup item in lookups)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_TAX_RATE, item.Item);
                    ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue taxRateItem = vault.ObjectPropertyOperations.GetProperty(taxRate, TAX_RATE_PD);

                    double taxPercentage;
                    double.TryParse(taxRateItem.TypedValue.DisplayValue.Replace("%", string.Empty), out taxPercentage);

                    lineTaxAmount = (lineNetAmount * taxPercentage) / 100;

                    PropertyValues prpsTaxDetail = new PropertyValues();

                    PropertyValue prpTaxClass = new PropertyValue();
                    prpTaxClass.PropertyDef = 100;
                    prpTaxClass.TypedValue.SetValue(MFDataType.MFDatatypeLookup, 246);
                    prpsTaxDetail.Add(-1, prpTaxClass);

                    PropertyValue prpTaxRate = new PropertyValue();
                    prpTaxRate.PropertyDef = TAX_DETAIL_TAX_RATE_PD;
                    prpTaxRate.TypedValue.SetValue(MFDataType.MFDatatypeLookup, item.Item);
                    prpsTaxDetail.Add(-1, prpTaxRate);

                    PropertyValue prpTaxAmount = new PropertyValue();
                    prpTaxAmount.PropertyDef = TAX_AMOUNT_PD;
                    prpTaxAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineTaxAmount.ToString("c", culture));
                    prpsTaxDetail.Add(-1, prpTaxAmount);

                    var obj = vault.ObjectOperations.CreateNewObject(OT_TAXT_DETAIL, prpsTaxDetail);
                    var checkinObj = vault.ObjectOperations.CheckIn(obj.ObjVer);
                    taxDetailLookup.Add(checkinObj.ObjVer.ObjID.ID);

                }

                PropertyValue prpTaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);
                prpTaxDetails.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                vault.ObjectPropertyOperations.SetProperty(objVer, prpTaxDetails);
            }
            else
            {
                if (prpLineTotal.TypedValue.DisplayValue != lineTotalAmount.ToString() && lineNetAmount != 0)
                {
                    lineTotalAmount = lineNetAmount;
                }
                else if (prpLineNetAmount.TypedValue.DisplayValue != lineNetAmount.ToString() && lineTotalAmount != 0)
                {
                    lineNetAmount = lineTotalAmount;
                }
                else  if (lineNetAmount == 0 && lineTotalAmount != 0)
                {
                    lineNetAmount = lineTotalAmount;
                }
                else if (lineTotalAmount == 0 && lineNetAmount != 0)
                {
                    lineTotalAmount = lineNetAmount;
                }
                if (prptaxDetails?.Value.IsNULL() == false)
                {
                    Lookups lookups = prptaxDetails.TypedValue.GetValueAsLookups();
                    foreach (Lookup item in lookups)
                    {
                        objId = new ObjID();
                        objId.SetIDs(OT_TAXT_DETAIL, item.Item);
                        ObjVer objTaxDetail = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                        taxDetailLookup.Remove(objTaxDetail.ObjID.ID);
                    }
                    PropertyValue prpTaxDetails = GetProperty(env, AP_InvoiceLine_TAX_Details_PD);
                    prpTaxDetails.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                    vault.ObjectPropertyOperations.SetProperty(objVer, prpTaxDetails);
                }
            }

            prpLineTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, lineTotalAmount.ToString("c", culture));
            vault.ObjectPropertyOperations.SetProperty(objVer, prpLineTotal);

            prpLineNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, lineNetAmount.ToString("c", culture));
            vault.ObjectPropertyOperations.SetProperty(objVer, prpLineNetAmount);

            //updating line count

            conditions = new SearchConditions();
            searchCondtion = new SearchCondition();
            searchCondtion.Expression.SetPropertyValueExpression(invoiceProperty, MFParentChildBehavior.MFParentChildBehaviorNone);
            searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, parent.Value.GetLookupID());
            searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;

            conditions.Add(-1, searchCondtion);

            searchCondtion = new SearchCondition();
            searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
            searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
            searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)lineObjectType);

            conditions.Add(-1, searchCondtion);

            searchCondtion = new SearchCondition();
            searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
            searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeDeleted, null);
            searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeBoolean, false);

            conditions.Add(-1, searchCondtion);

            var existingLineItems = vault.ObjectSearchOperations
               .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
               .Cast<ObjectVersion>()
               .ToList();

            int lineCount = existingLineItems.Count;
            double parentTotalAmount = 0;
            double parentNetAmount = 0;

            Dictionary<int, double> trWiseTaxTotal = new Dictionary<int, double>();
            taxDetailLookup = new List<int>();

            foreach (ObjectVersion item in existingLineItems)
            {
                double exitingLineTotalAmount = 0;
                double exitingLineNetAmount = 0;

                PropertyValue prpExistingLineTotal = GetProperty(vault, item, AP_Invoice_TotalAmount_PropertyDef);
                double.TryParse(prpExistingLineTotal.Value.DisplayValue, NumberStyles.Currency, culture, out exitingLineTotalAmount);
                parentTotalAmount += exitingLineTotalAmount;

                PropertyValue prpExistingNet = GetProperty(vault, item, Net_Line_Amount_PD);
                double.TryParse(prpExistingNet.Value.DisplayValue, NumberStyles.Currency, culture, out exitingLineNetAmount);
                parentNetAmount += exitingLineNetAmount;

                taxGroupValue = GetProperty(env.Vault, item.ObjVer, AP_InvoiceLine_TAX_Group_PD);

                if (taxGroupValue.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_TAX_GROUP, taxGroupValue.Value.GetLookupID());
                    ObjVer taxGroup = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue pvTaxRates = GetProperty(env.Vault, taxGroup, AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD);

                    Lookups lookups = pvTaxRates.TypedValue.GetValueAsLookups();

                    foreach (Lookup lookup in lookups)
                    {
                        objId = new ObjID();
                        objId.SetIDs(OT_TAX_RATE, lookup.Item);
                        ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                        double taxRateAmount = 0;
                        Double.TryParse(GetProperty(vault, taxRate, TAX_RATE_PD).TypedValue.Value.ToString().Replace("%", ""), out taxRateAmount);
                        double tax;

                        int decimalPlace;
                        PropertyValue taxDecimalPlace = vault.ObjectPropertyOperations.GetProperty(taxRate, DECIMAL_PLACE_PD);
                        int.TryParse(taxDecimalPlace.TypedValue.DisplayValue, out decimalPlace);

                        tax = exitingLineNetAmount * taxRateAmount / 100;

                        tax = Math.Round(tax, decimalPlace);

                        if (trWiseTaxTotal.ContainsKey(objId.ID))
                        {
                            trWiseTaxTotal[objId.ID] += tax;
                        }
                        else
                        {
                            trWiseTaxTotal.Add(objId.ID, tax);
                        }
                    }
                }
            }
            //updating parent item
            if (parent.Value.IsNULL() == false)
            {
                objId = new ObjID();
                objId.SetIDs(headerObjectType, parent.Value.GetLookupID());
                IsCalledFromLine = true;

                PropertyValue prpParentTotal = GetProperty(vault, parentObject, AP_Invoice_TotalAmount_PropertyDef);
                //PropertyValue prpParentRemaining = GetProperty(vault, parentObject, B_PD_Remaining_Balance);
                PropertyValue prpParentNetAmount = GetProperty(vault, parentObject, AP_Invoice_NetAmount_PropertyDef);

                prpParentTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, parentTotalAmount.ToString("c", culture));
                //prpParentRemaining.TypedValue.SetValue(MFDataType.MFDatatypeText, parentTotalAmount.ToString("c", culture));
                prpParentNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, parentNetAmount.ToString("c", culture));

                var objectVersionToBeUpdated = vault.ObjectOperations.CheckOut(objId);

                if (trWiseTaxTotal.Count > 0)
                {
                    PropertyValue prpParentTaxDetail = GetProperty(vault, parentObject, AP_Invoice_TaxDetails_PropertyDef);

                    foreach (var dicItem in trWiseTaxTotal)
                    {
                        PropertyValues prpsTaxDetail = new PropertyValues();

                        PropertyValue prpTaxClass = new PropertyValue();
                        prpTaxClass.PropertyDef = 100;
                        prpTaxClass.TypedValue.SetValue(MFDataType.MFDatatypeLookup, 246);
                        prpsTaxDetail.Add(-1, prpTaxClass);

                        PropertyValue prpTaxRate = new PropertyValue();
                        prpTaxRate.PropertyDef = TAX_DETAIL_TAX_RATE_PD;
                        prpTaxRate.TypedValue.SetValue(MFDataType.MFDatatypeLookup, dicItem.Key);
                        prpsTaxDetail.Add(-1, prpTaxRate);

                        PropertyValue prpTaxAmount = new PropertyValue();
                        prpTaxAmount.PropertyDef = TAX_AMOUNT_PD;
                        prpTaxAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, dicItem.Value.ToString("c", culture));
                        prpsTaxDetail.Add(-1, prpTaxAmount);

                        var obj = vault.ObjectOperations.CreateNewObject(OT_TAXT_DETAIL, prpsTaxDetail);
                        var checkinObj = vault.ObjectOperations.CheckIn(obj.ObjVer);
                        taxDetailLookup.Add(checkinObj.ObjVer.ObjID.ID);
                    }
                    prpParentTaxDetail.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                    vault.ObjectPropertyOperations.SetProperty(objectVersionToBeUpdated.ObjVer, prpParentTaxDetail);
                }

                vault.ObjectPropertyOperations.SetProperty(objectVersionToBeUpdated.ObjVer, prpParentTotal);
                vault.ObjectPropertyOperations.SetProperty(objectVersionToBeUpdated.ObjVer, prpParentNetAmount);
                // vault.ObjectPropertyOperations.SetProperty(objectVersionToBeUpdated.ObjVer, prpParentRemaining);
                vault.ObjectOperations.CheckIn(objectVersionToBeUpdated.ObjVer);
            }

            SysUtils.ReportToEventLog("END: TotalUpdate", System.Diagnostics.EventLogEntryType.Information);
        }

        bool isHeaderDeleted = false;

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDeleteObject, ObjectType = "B.OT.AP_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, ObjectType = "B.OT.AP_Invoice")]
        //[EventHandler(MFEventHandlerType.MFEventHandlerBeforeDeleteObject, ObjectType = "B.OT.AR_Invoice")]
        //[EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, ObjectType = "B.OT.AR_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDeleteObject, Class = "B.DC.AR_Invoice")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, Class = "B.DC.AR_Invoice")]

        public void onInvoiceDeleted(EventHandlerEnvironment env)
        {
            isHeaderDeleted = true;
        }

        [EventHandler(MFEventHandlerType.MFEventHandlerAfterDeleteObject, Class = "B.OC.AP_Line_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, Class = "B.OC.AP_Line_Item")]
        private void onAPLineDeleted(EventHandlerEnvironment env)
        {
            onLineUpdatedOrDeleted(env, 0, OT_AP_INVOICE_LINE, B_PD_AP_Invoice);
        }


        [EventHandler(MFEventHandlerType.MFEventHandlerAfterDeleteObject, Class = "B.OC.AR_Line_Item")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, Class = "B.OC.AR_Line_Item")]
        private void onARLineDeleted(EventHandlerEnvironment env)
        {
            onLineUpdatedOrDeleted(env, 0, OT_AR_INVOICE_LINE, B_PD_AR_Invoice);
        }


        private void onLineUpdatedOrDeleted(EventHandlerEnvironment env, int headerObjectType, int lineObjectType, int invoiceProperty)
        {
            try
            {
                if (isHeaderDeleted)
                {
                    isHeaderDeleted = false;
                    return;
                }

                SysUtils.ReportToEventLog("Start: onLineUpdatedOrDeleted", System.Diagnostics.EventLogEntryType.Information);

                Vault vault = env.Vault;
                PropertyValue parent = GetProperty(env, invoiceProperty);

                SearchConditions conditions = new SearchConditions();
                SearchCondition searchCondtion = new SearchCondition();
                searchCondtion.Expression.SetPropertyValueExpression(invoiceProperty, MFParentChildBehavior.MFParentChildBehaviorNone);
                searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, parent.Value.GetLookupID());
                searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                conditions.Add(-1, searchCondtion);

                searchCondtion = new SearchCondition();
                searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeObjectTypeID, null);
                searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeLookup, (int)lineObjectType);
                conditions.Add(-1, searchCondtion);

                searchCondtion = new SearchCondition();
                searchCondtion.ConditionType = MFConditionType.MFConditionTypeEqual;
                searchCondtion.Expression.SetStatusValueExpression(MFStatusType.MFStatusTypeDeleted, null);
                searchCondtion.TypedValue.SetValue(MFDataType.MFDatatypeBoolean, false);

                conditions.Add(-1, searchCondtion);

                ObjectVersions objVersions = vault.ObjectSearchOperations
                    .SearchForObjectsByConditions(conditions, MFSearchFlags.MFSearchFlagLookAllObjectTypes, true)
                    .GetAsObjectVersions();


                ObjID objId = new ObjID();
                objId.SetIDs(headerObjectType, parent.Value.GetLookupID());
                ObjVer parentObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                PropertyValue prpParentTotal = GetProperty(vault, parentObject, AP_Invoice_TotalAmount_PropertyDef);
                PropertyValue prpParentNetAmount = GetProperty(vault, parentObject, AP_Invoice_NetAmount_PropertyDef);

                double parentTotalAmount = 0;
                double parentNetAmount = 0;

                PropertyValue prpParentCurrency = GetProperty(env.Vault, parentObject, B_PD_Currency);
                CultureInfo culture = null;
                if (prpParentCurrency.Value.IsNULL() == false)
                {
                    objId = new ObjID();
                    objId.SetIDs(OT_CURRENCY, prpParentCurrency.Value.GetLookupID());
                    ObjVer currencyObject = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                    PropertyValue prpParentCulture = GetProperty(env.Vault, currencyObject, B_PD_Culture);
                    culture = new CultureInfo(prpParentCulture.TypedValue.DisplayValue);
                }
                else
                {
                    culture = new CultureInfo("en-US");
                }

                Dictionary<int, double> trWiseTaxTotal = new Dictionary<int, double>();
                List<int> taxDetailLookup = new List<int>();

                PropertyValue taxGroupValue = GetProperty(env, AP_InvoiceLine_TAX_Group_PD);

                foreach (ObjectVersion item in objVersions)
                {
                    if (item.ObjVer.ID == env.ObjVer.ID)
                        continue;

                    IsValidCheckIn = false;

                    //var objectVersionToBeUpdated = vault.ObjectOperations.CheckOut(item.ObjVer.ObjID);
                    //PropertyValue prpLineNo = GetProperty(vault, objectVersionToBeUpdated, Line_PD);
                    //prpLineNo.TypedValue.SetValue(MFDataType.MFDatatypeInteger, lineNumber);
                    //vault.ObjectPropertyOperations.SetProperty(objectVersionToBeUpdated.ObjVer, prpLineNo);
                    //vault.ObjectOperations.CheckIn(objectVersionToBeUpdated.ObjVer);
                    //lineNumber++;

                    double exitingLineTotalAmount = 0;
                    PropertyValue prpExistingLineTotal = GetProperty(vault, item, AP_Invoice_TotalAmount_PropertyDef);
                    double.TryParse(prpExistingLineTotal.Value.DisplayValue, NumberStyles.Currency, culture, out exitingLineTotalAmount);
                    parentTotalAmount += exitingLineTotalAmount;

                    double exitingLineNetAmount = 0;
                    PropertyValue prpExistingNet = GetProperty(vault, item, Net_Line_Amount_PD);
                    double.TryParse(prpExistingNet.Value.DisplayValue, NumberStyles.Currency, culture, out exitingLineNetAmount);
                    parentNetAmount += exitingLineNetAmount;

                    taxGroupValue = GetProperty(env.Vault, item.ObjVer, AP_InvoiceLine_TAX_Group_PD);

                    if (taxGroupValue.Value.IsNULL() == false)
                    {
                        objId = new ObjID();
                        objId.SetIDs(OT_TAX_GROUP, taxGroupValue.Value.GetLookupID());
                        ObjVer taxGroup = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                        PropertyValue pvTaxRates = GetProperty(env.Vault, taxGroup, AP_InvoiceLine_TAX_GROUP_TAX_RATES_PD);

                        Lookups lookups = pvTaxRates.TypedValue.GetValueAsLookups();

                        foreach (Lookup lookup in lookups)
                        {
                            objId = new ObjID();
                            objId.SetIDs(OT_TAX_RATE, lookup.Item);
                            ObjVer taxRate = vault.ObjectOperations.GetLatestObjVer(objId, false, true);

                            double taxRateAmount = 0;
                            Double.TryParse(GetProperty(vault, taxRate, TAX_RATE_PD).TypedValue.Value.ToString().Replace("%", ""), out taxRateAmount);
                            double tax;

                            tax = exitingLineNetAmount * taxRateAmount / 100;

                            if (trWiseTaxTotal.ContainsKey(objId.ID))
                            {
                                trWiseTaxTotal[objId.ID] += tax;
                            }
                            else
                            {
                                trWiseTaxTotal.Add(objId.ID, tax);
                            }
                        }
                    }

                }

                prpParentTotal.TypedValue.SetValue(MFDataType.MFDatatypeText, parentTotalAmount.ToString("c", culture));
                prpParentNetAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, parentNetAmount.ToString("c", culture));

                var parentObjectVersionToBeUpdated = vault.ObjectOperations.CheckOut(parentObject.ObjID);

                if (trWiseTaxTotal.Count > 0)
                {
                    PropertyValue prpParentTaxDetail = GetProperty(vault, parentObject, AP_Invoice_TaxDetails_PropertyDef);
                    foreach (var dicItem in trWiseTaxTotal)
                    {
                        PropertyValues prpsTaxDetail = new PropertyValues();

                        PropertyValue prpTaxClass = new PropertyValue();
                        prpTaxClass.PropertyDef = 100;
                        prpTaxClass.TypedValue.SetValue(MFDataType.MFDatatypeLookup, 246);
                        prpsTaxDetail.Add(-1, prpTaxClass);

                        PropertyValue prpTaxRate = new PropertyValue();
                        prpTaxRate.PropertyDef = TAX_DETAIL_TAX_RATE_PD;
                        prpTaxRate.TypedValue.SetValue(MFDataType.MFDatatypeLookup, dicItem.Key);
                        prpsTaxDetail.Add(-1, prpTaxRate);

                        PropertyValue prpTaxAmount = new PropertyValue();
                        prpTaxAmount.PropertyDef = TAX_AMOUNT_PD;
                        prpTaxAmount.TypedValue.SetValue(MFDataType.MFDatatypeText, dicItem.Value.ToString("c", culture));
                        prpsTaxDetail.Add(-1, prpTaxAmount);

                        var obj = vault.ObjectOperations.CreateNewObject(OT_TAXT_DETAIL, prpsTaxDetail);
                        var checkinObj = vault.ObjectOperations.CheckIn(obj.ObjVer);
                        taxDetailLookup.Add(checkinObj.ObjVer.ObjID.ID);
                    }

                    prpParentTaxDetail.TypedValue.SetValue(MFDataType.MFDatatypeMultiSelectLookup, taxDetailLookup.ToArray());
                    vault.ObjectPropertyOperations.SetProperty(parentObjectVersionToBeUpdated.ObjVer, prpParentTaxDetail);
                }

                vault.ObjectPropertyOperations.SetProperty(parentObjectVersionToBeUpdated.ObjVer, prpParentTotal);
                vault.ObjectPropertyOperations.SetProperty(parentObjectVersionToBeUpdated.ObjVer, prpParentNetAmount);
                vault.ObjectOperations.CheckIn(parentObjectVersionToBeUpdated.ObjVer);

                IsCalledFromLine = true;
                IsValidCheckIn = true;
                SysUtils.ReportToEventLog("END: onLineUpdatedOrDeleted", System.Diagnostics.EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                SysUtils.ReportToEventLog("Error occurred in onLineUpdatedOrDeleted" + ex.Message, System.Diagnostics.EventLogEntryType.Information);
            }
            finally
            {
                IsValidCheckIn = true;
            }
        }

        private PropertyValue GetProperty(EventHandlerEnvironment env, int pd)
        {
            if (env.EventType == MFEventHandlerType.MFEventHandlerBeforeCheckInChanges
                || env.EventType == MFEventHandlerType.MFEventHandlerAfterDeleteObject
                || env.EventType == MFEventHandlerType.MFEventHandlerBeforeDestroyObject
                || env.EventType == MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize)
            {
                PropertyValue prp = env.Vault.ObjectPropertyOperations.GetProperty(env.ObjVer, pd);
                if (prp == null)
                {
                    SysUtils.ReportToEventLog("PropertyValue not found :" + pd, System.Diagnostics.EventLogEntryType.Information);
                }

                return prp;
            }

            PropertyValue returnPrp = null;
            env.PropertyValues.TryGetProperty(pd, out returnPrp);

            if (returnPrp == null)
            {
                SysUtils.ReportToEventLog("PropertyValue not found for the definition : " + pd.ToString(), System.Diagnostics.EventLogEntryType.Information);
            }

            return returnPrp;
        }

        private PropertyValue GetProperty(Vault vault, ObjectVersion objVersion, int pd)
        {
            PropertyValue prp = vault.ObjectPropertyOperations.GetProperty(objVersion.ObjVer, pd);
            if (prp == null)
            {
                SysUtils.ReportToEventLog("PropertyValue not found :" + pd, System.Diagnostics.EventLogEntryType.Information);
            }

            return prp;
        }

        private PropertyValue GetProperty(Vault vault, ObjVer objVersion, int pd)
        {
            PropertyValue prp = vault.ObjectPropertyOperations.GetProperty(objVersion, pd);
            if (prp == null)
            {
                SysUtils.ReportToEventLog("PropertyValue not found :" + pd, System.Diagnostics.EventLogEntryType.Information);
            }

            return prp;
        }

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Phone_Type = "B.PD.Phone_Type";

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Mobile_Phone = "B.PD.Mobile_Number";

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Direct_Phone = "B.PD.Direct_Number";

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Business_Phone = "B.PD.Business_Number";

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Home_Phone = "B.PD.Home_Number";

        [MFObjType(Required = true)]
        MFIdentifier OT_Contact = "B.OT.Contact";

        [MFObjType(Required = true)]
        MFIdentifier OT_Phone_Number = "B.OT.Phone_Number";

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, ObjectType = "B.OT.Phone_Number")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, ObjectType = "B.OT.Phone_Number")]
        public void onMobileNumberCreated(EventHandlerEnvironment env)
        {
            try
            {
                SysUtils.ReportToEventLog("onMobileNumberCreated :Started", System.Diagnostics.EventLogEntryType.Error);

                PropertyValue prpMobileType = GetProperty(env, PD_Phone_Type);
                PropertyValue prpParentContact = GetProperty(env, GetObjectTypeOwnerPropertyID(env.Vault, OT_Phone_Number));

                if (prpParentContact != null && prpParentContact.Value.IsNULL() == false)
                {
                    ObjID objId = new ObjID();
                    objId.SetIDs(OT_Contact, prpParentContact.Value.GetLookupID());
                    ObjVer contact = env.Vault.ObjectOperations.GetLatestObjVer(objId, true, true);

                    PropertyValue prpContactToBeSet = null;
                    PropertyValue prpContactToBeCleared = null;
                    PropertyValue existingMobileType = null;

                    if (env.ObjVer.Version - 1 > 0)
                    {
                        ObjVer oldVersion = new ObjVer();
                        oldVersion.SetIDs(OT_Phone_Number, env.ObjVer.ObjID.ID, env.ObjVer.Version - 1);
                        existingMobileType = env.Vault.ObjectPropertyOperations.GetProperty(oldVersion, PD_Phone_Type);
                    }

                    if (prpMobileType.TypedValue.DisplayValue == "Mobile")
                        prpContactToBeSet = GetProperty(env.Vault, contact, PD_Mobile_Phone);

                    if (prpMobileType.TypedValue.DisplayValue == "Business")
                        prpContactToBeSet = GetProperty(env.Vault, contact, PD_Business_Phone);

                    if (prpMobileType.TypedValue.DisplayValue == "Direct")
                        prpContactToBeSet = GetProperty(env.Vault, contact, PD_Direct_Phone);

                    if (prpMobileType.TypedValue.DisplayValue == "Home")
                        prpContactToBeSet = GetProperty(env.Vault, contact, PD_Home_Phone);

                    if (existingMobileType != null && existingMobileType.Value.DisplayValue != prpMobileType.Value.DisplayValue)
                    {
                        if (existingMobileType.TypedValue.DisplayValue == "Mobile")
                            prpContactToBeCleared = GetProperty(env.Vault, contact, PD_Mobile_Phone);

                        if (existingMobileType.TypedValue.DisplayValue == "Business")
                            prpContactToBeCleared = GetProperty(env.Vault, contact, PD_Business_Phone);

                        if (existingMobileType.TypedValue.DisplayValue == "Direct")
                            prpContactToBeCleared = GetProperty(env.Vault, contact, PD_Direct_Phone);

                        if (existingMobileType.TypedValue.DisplayValue == "Home")
                            prpContactToBeCleared = GetProperty(env.Vault, contact, PD_Home_Phone);
                    }

                    ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(contact.ObjID);

                    bool canBeCheckedIn = false;

                    if (prpContactToBeSet != null && prpContactToBeSet.Value.GetLookupID() != env.ObjVer.ObjID.ID)
                    {
                        canBeCheckedIn = true;
                        prpContactToBeSet.Value.SetValue(MFDataType.MFDatatypeLookup, env.ObjVer.ObjID.ID);
                        env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpContactToBeSet);
                    }

                    if (prpContactToBeCleared != null)
                    {
                        canBeCheckedIn = true;
                        prpContactToBeCleared.Value.SetValue(MFDataType.MFDatatypeLookup, null);
                        env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpContactToBeCleared);
                    }

                    if (canBeCheckedIn)
                        env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
                    else
                        env.Vault.ObjectOperations.UndoCheckout(objectToBeUpdated.ObjVer);

                    SysUtils.ReportToEventLog("onMobileNumberCreated :Completed", System.Diagnostics.EventLogEntryType.Error);

                }
            }
            catch (Exception ex)
            {
                SysUtils.ReportToEventLog("Error on onMobileNumberCreated :" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }

        }

        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, ObjectType = "B.OT.Phone_Number")]
        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeDeleteObject, ObjectType = "B.OT.Phone_Number")]
        public void onMobileNumberDeleted(EventHandlerEnvironment env)
        {
            try
            {
                PropertyValue prpMobileType = GetProperty(env.Vault, env.ObjVer, PD_Phone_Type);
                PropertyValue prpParentContact = GetProperty(env.Vault, env.ObjVer, GetObjectTypeOwnerPropertyID(env.Vault, OT_Phone_Number));

                if (prpParentContact.Value.IsNULL() == false)
                {
                    ObjID objId = new ObjID();
                    objId.SetIDs(OT_Contact, prpParentContact.Value.GetLookupID());
                    ObjVer contact = env.Vault.ObjectOperations.GetLatestObjVer(objId, true, true);
                    PropertyValue prpParentPhone = null;

                    if (prpMobileType.TypedValue.DisplayValue == "Mobile")
                        prpParentPhone = GetProperty(env.Vault, contact, PD_Mobile_Phone);

                    if (prpMobileType.TypedValue.DisplayValue == "Business")
                        prpParentPhone = GetProperty(env.Vault, contact, PD_Business_Phone);

                    if (prpMobileType.TypedValue.DisplayValue == "Direct")
                        prpParentPhone = GetProperty(env.Vault, contact, PD_Direct_Phone);

                    if (prpMobileType.TypedValue.DisplayValue == "Home")
                        prpParentPhone = GetProperty(env.Vault, contact, PD_Home_Phone);

                    if (prpParentPhone == null)
                        return;

                    prpParentPhone.Value.SetValue(MFDataType.MFDatatypeLookup, null);
                    ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(contact.ObjID);
                    env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpParentPhone);
                    env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
                }
            }
            catch (Exception ex)
            {
                SysUtils.ReportToEventLog("Error on onMobileNumberDeleted :" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }

        }

        private int GetObjectTypeOwnerPropertyID(Vault vault, int iOT)
        {
            ObjType oObjType;
            int ownerObjectType = -1;
            oObjType = vault.ObjectTypeOperations.GetObjectType(iOT);

            if (oObjType.HasOwnerType)
            {
                ObjType oOwnerObjType;
                oOwnerObjType = vault.ObjectTypeOperations.GetObjectType(oObjType.OwnerType);
                ownerObjectType = oOwnerObjType.OwnerPropertyDef;
            }
            return ownerObjectType;
        }

        [MFPropertyDef(Required = true)]
        MFIdentifier PD_Address_Type = "B.PD.Address_Type";

        //[MFPropertyDef(Required = true)]
       // MFIdentifier PD_Shipping_Address = "B.PD.Shipping_Address";

       // [MFPropertyDef(Required = true)]
       // MFIdentifier PD_Billing_Address = "B.PD.Billing_Address";

        //[MFObjType]
        //MFIdentifier OT_Address_Of_Entity = "B.OT.Address_of_Entity";

        [MFObjType]
        MFIdentifier OT_Legal_Entity = "B.OT.Legal_Entity";


        //[EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize, ObjectType = "B.OT.Legal_Entity")]
        //[EventHandler(MFEventHandlerType.MFEventHandlerBeforeCheckInChanges, ObjectType = "B.OT.Legal_Entity")]
        //public void onAddressOfEntityCreated(EventHandlerEnvironment env)
        //{
        //    try
        //    {
        //        SysUtils.ReportToEventLog("onAddressOfEntityCreated :Started", System.Diagnostics.EventLogEntryType.Information);

        //        PropertyValue prpAddressType = GetProperty(env.Vault, env.ObjVer, PD_Address_Type);
        //        PropertyValue prpOwner = GetProperty(env.Vault, env.ObjVer, GetObjectTypeOwnerPropertyID(env.Vault, OT_Address_Of_Entity));

        //        if (prpOwner.Value.IsNULL() == false)
        //        {
        //            ObjID objId = new ObjID();
        //            objId.SetIDs(OT_Legal_Entity, prpOwner.Value.GetLookupID());
        //            ObjVer contact = env.Vault.ObjectOperations.GetLatestObjVer(objId, true, true);
        //            PropertyValue prpParentAddress = null;

        //            PropertyValue prpAddressToBeCleared = null;
        //            PropertyValue existingAddressType = null;

        //            if (env.ObjVer.Version - 1 > 0)
        //            {
        //                ObjVer oldVersion = new ObjVer();
        //                oldVersion.SetIDs(OT_Address_Of_Entity, env.ObjVer.ObjID.ID, env.ObjVer.Version - 1);
        //                existingAddressType = env.Vault.ObjectPropertyOperations.GetProperty(oldVersion, PD_Address_Type);
        //            }

        //            //if (prpAddressType.TypedValue.DisplayValue == "Shipping")
        //            //    prpParentAddress = GetProperty(env.Vault, contact, PD_Shipping_Address);

        //            //if (prpAddressType.TypedValue.DisplayValue == "Billing")
        //            //    prpParentAddress = GetProperty(env.Vault, contact, PD_Billing_Address);

        //            //if (existingAddressType != null && existingAddressType.Value.DisplayValue != prpAddressType.Value.DisplayValue)
        //            //{
        //            //    if (existingAddressType.TypedValue.DisplayValue == "Shipping")
        //            //        prpAddressToBeCleared = GetProperty(env.Vault, contact, PD_Shipping_Address);

        //            //    if (existingAddressType.TypedValue.DisplayValue == "Billing")
        //            //        prpAddressToBeCleared = GetProperty(env.Vault, contact, PD_Billing_Address);
        //            //}

        //            if (prpParentAddress == null)
        //                return;

        //            ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(contact.ObjID);

        //            bool canBeCheckedIn = false;

        //            if (prpParentAddress != null && prpParentAddress.Value.GetLookupID() != env.ObjVer.ObjID.ID)
        //            {
        //                canBeCheckedIn = true;
        //                prpParentAddress.Value.SetValue(MFDataType.MFDatatypeLookup, env.ObjVer.ObjID.ID);
        //                env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpParentAddress);
        //            }

        //            if (prpAddressToBeCleared != null)
        //            {
        //                canBeCheckedIn = true;
        //                prpAddressToBeCleared.Value.SetValue(MFDataType.MFDatatypeLookup, null);
        //                env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpAddressToBeCleared);
        //            }

        //            if (canBeCheckedIn)
        //                env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
        //            else
        //                env.Vault.ObjectOperations.UndoCheckout(objectToBeUpdated.ObjVer);
        //        }

        //        SysUtils.ReportToEventLog("onAddressOfEntityCreated :Completed", System.Diagnostics.EventLogEntryType.Information);
        //    }
        //    catch (Exception ex)
        //    {
        //        SysUtils.ReportToEventLog("Error on onAddressOfEntityCreated :" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
        //    }
        //}

        //[EventHandler(MFEventHandlerType.MFEventHandlerBeforeDestroyObject, ObjectType = "B.OT.Legal_Entity")]
        //[EventHandler(MFEventHandlerType.MFEventHandlerBeforeDeleteObject, ObjectType = "B.OT.Legal_Entity")]
        //public void onAddressOfEntityDeleted(EventHandlerEnvironment env)
        //{
        //    try
        //    {
        //        SysUtils.ReportToEventLog("onAddressOfEntityDeleted :Started", System.Diagnostics.EventLogEntryType.Information);

        //        PropertyValue prpAddressType = GetProperty(env.Vault, env.ObjVer, PD_Address_Type);
        //        PropertyValue prpParent = GetProperty(env.Vault, env.ObjVer, GetObjectTypeOwnerPropertyID(env.Vault, OT_Address_Of_Entity));

        //        if (prpParent.Value.IsNULL() == false)
        //        {
        //            ObjID objId = new ObjID();
        //            objId.SetIDs(OT_Legal_Entity, prpParent.Value.GetLookupID());
        //            ObjVer legalEntity = env.Vault.ObjectOperations.GetLatestObjVer(objId, true, true);
        //            PropertyValue prpParentPhone = null;

        //            //if (prpAddressType.TypedValue.DisplayValue == "Billing")
        //            //    prpParentPhone = GetProperty(env.Vault, legalEntity, PD_Billing_Address);

        //            //if (prpAddressType.TypedValue.DisplayValue == "Shipping")
        //            //    prpParentPhone = GetProperty(env.Vault, legalEntity, PD_Shipping_Address);

        //            //if (prpParentPhone == null)
        //            //    return;

        //            prpParentPhone.Value.SetValue(MFDataType.MFDatatypeLookup, null);
        //            ObjectVersion objectToBeUpdated = env.Vault.ObjectOperations.CheckOut(legalEntity.ObjID);
        //            env.Vault.ObjectPropertyOperations.SetProperty(objectToBeUpdated.ObjVer, prpParentPhone);
        //            env.Vault.ObjectOperations.CheckIn(objectToBeUpdated.ObjVer);
        //        }

        //        SysUtils.ReportToEventLog("onAddressOfEntityDeleted :Started", System.Diagnostics.EventLogEntryType.Information);
        //    }
        //    catch (Exception ex)
        //    {
        //        SysUtils.ReportToEventLog("Error on onAddressOfEntityDeleted :" + ex.Message, System.Diagnostics.EventLogEntryType.Error);
        //    }

        //}

        private void GetDecimalFromString(EventHandlerEnvironment env, string displayValue, CultureInfo culture, out double lineAmount)
        {
            lineAmount = 0.0;
            if (displayValue != "0")
            {
                double.TryParse(displayValue, NumberStyles.Currency, culture, out lineAmount);
                if (lineAmount != 0)
                {
                    return;
                }
            }
            ValueListItems items = env.Vault.ValueListItemOperations.GetValueListItems(VL_CULTURES);
            foreach (ValueListItem item in items)
            {
                CultureInfo cultureitem = new CultureInfo(item.Name);
                double.TryParse(displayValue, NumberStyles.Currency, cultureitem, out lineAmount);
                if (lineAmount != 0)
                {
                    break;
                }
            }
        }

    }
}